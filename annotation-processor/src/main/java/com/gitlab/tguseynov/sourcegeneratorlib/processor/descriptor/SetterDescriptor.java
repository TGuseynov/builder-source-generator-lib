package com.gitlab.tguseynov.sourcegeneratorlib.processor.descriptor;

/**
 * Descriptor for a setter method
 */
public class SetterDescriptor
{
	private String returnType;
	private FieldDescriptor field;

	public SetterDescriptor()
	{

	}

	/**
	 * @return class name of a setter return type
	 */
	public String getReturnType()
	{
		return returnType;
	}

	/**
	 * @param returnType
	 * 		class name of a setter return type
	 */
	public void setReturnType(String returnType)
	{
		this.returnType = returnType;
	}

	/**
	 * @return descriptor of a field that setter is setting value
	 *
	 * @see FieldDescriptor
	 */
	public FieldDescriptor getField()
	{
		return field;
	}

	/**
	 * @param field
	 * 		descriptor of a field that setter is setting value
	 *
	 * @see FieldDescriptor
	 */
	public void setField(FieldDescriptor field)
	{
		this.field = field;
	}
}
