/*
 * Copyright (c) Watcher, 2015.
 */

package com.gitlab.tguseynov.sourcegeneratorlib.processor.velocity;

import com.gitlab.tguseynov.sourcegeneratorlib.processor.descriptor.ClassDescriptor;
import org.apache.velocity.VelocityContext;
import org.apache.velocity.app.VelocityEngine;

import java.io.Writer;
import java.util.Properties;

/**
 * Facade to interact with Velocity library
 */
public class VelocityFacade
{
	private static VelocityFacade instance = null;

	public static final String VELOCITY_PROPERTIES_FILE_NAME = "properties/velocity.properties";
	public static final String VELOCITY_TEMPLATE_FILE_NAME = "template/builder/builder.vm";

	private VelocityEngine engine;

	private VelocityFacade()
	{

	}

	/**
	 * Maps {@link ClassDescriptor} properties to Velocity template and writes it to a source file
	 *
	 * @param writer
	 * 		instance of a writer
	 * @param descriptor
	 * 		builder class descriptor
	 * @param velocityProperties
	 * 		Velocity properties from properties resource file
	 *
	 * @return <b>TRUE</b> if source was successfully written
	 */
	public boolean mergeTemplate(Writer writer, ClassDescriptor descriptor, Properties velocityProperties)
	{
		VelocityEngine engine = getEngine(velocityProperties);
		VelocityContext context = createContext(descriptor);
		return engine.mergeTemplate(VELOCITY_TEMPLATE_FILE_NAME, "UTF-8", context, writer);
	}

	/**
	 * Initializes {@link VelocityEngine} with given properties
	 *
	 * @param properties
	 * 		Velocity properties from properties resource file
	 *
	 * @return initialized {@link VelocityEngine}
	 */
	private VelocityEngine getEngine(Properties properties)
	{
		if (engine == null)
		{
			engine = new VelocityEngine(properties);
			engine.init();
		}
		return engine;
	}

	/**
	 * Creates {@link VelocityContext} and puts {@link ClassDescriptor} properties to its map
	 *
	 * @param descriptor
	 * 		builder class descriptor
	 *
	 * @return {@link VelocityContext} with mapped descriptor
	 */
	private VelocityContext createContext(ClassDescriptor descriptor)
	{
		VelocityContext context = new VelocityContext();
		context.put("descriptor", descriptor);
		return context;
	}

	/**
	 * @return {@link VelocityFacade} instance
	 */
	public static VelocityFacade getInstance()
	{
		if (instance == null)
		{
			instance = new VelocityFacade();
		}
		return instance;
	}
}
