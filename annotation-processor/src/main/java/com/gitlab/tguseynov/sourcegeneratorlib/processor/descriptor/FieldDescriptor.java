package com.gitlab.tguseynov.sourcegeneratorlib.processor.descriptor;

/**
 * Descriptor of a field of a builder class
 */
public class FieldDescriptor
{
	private String name;
	private String type;

	public FieldDescriptor()
	{

	}

	/**
	 * @return field name
	 */
	public String getName()
	{
		return name;
	}

	/**
	 * @param name
	 * 		field name
	 */
	public void setName(String name)
	{
		this.name = name;
	}

	/**
	 * @return class name field type
	 */
	public String getType()
	{
		return type;
	}

	/**
	 * @param type
	 * 		class name field type
	 */
	public void setType(String type)
	{
		this.type = type;
	}
}
