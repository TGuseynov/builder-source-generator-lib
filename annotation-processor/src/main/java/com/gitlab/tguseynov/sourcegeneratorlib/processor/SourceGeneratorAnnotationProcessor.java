package com.gitlab.tguseynov.sourcegeneratorlib.processor;

import com.gitlab.tguseynov.sourcegeneratorlib.builder.annotation.*;
import com.gitlab.tguseynov.sourcegeneratorlib.builder.annotation.Optional;
import com.gitlab.tguseynov.sourcegeneratorlib.processor.descriptor.ClassDescriptor;
import com.gitlab.tguseynov.sourcegeneratorlib.processor.descriptor.FieldDescriptor;
import com.gitlab.tguseynov.sourcegeneratorlib.processor.descriptor.NestedInterfaceDescriptor;
import com.gitlab.tguseynov.sourcegeneratorlib.processor.descriptor.SetterDescriptor;
import com.gitlab.tguseynov.sourcegeneratorlib.processor.logger.AnnotationProcessorLogger;

import javax.annotation.processing.*;
import javax.lang.model.SourceVersion;
import javax.lang.model.element.*;
import javax.tools.JavaFileObject;
import java.io.IOException;
import java.io.Writer;
import java.util.*;
import java.lang.annotation.Annotation;

/**
 * Annotation processor for Builder annotations. If you want an entity to be processed and have a generated Builder
 * class than a class should be annotated with {@link BuildObject} or {@link SpringBuildObject} annotation
 *
 * @see BuildObject
 * @see SpringBuildObject
 * @see Ignore
 * @see Mandatory
 * @see Optional
 */
@SupportedAnnotationTypes({
		"com.gitlab.tguseynov.buildersourcegeneratorlib.builder.annotation.BuildObject",
		"com.gitlab.tguseynov.buildersourcegeneratorlib.builder.annotation.SpringBuildObject"
})
@SupportedSourceVersion(SourceVersion.RELEASE_8)
public class SourceGeneratorAnnotationProcessor extends AbstractProcessor
{
	private AnnotationProcessorLogger logger;

	public SourceGeneratorAnnotationProcessor()
	{
		super();
	}

	@Override
	public synchronized void init(ProcessingEnvironment processingEnv)
	{
		super.init(processingEnv);

		logger = AnnotationProcessorLogger.getInstance();
		logger.setMessager(processingEnv.getMessager());
	}

	@Override
	@SuppressWarnings("unchecked")
	public boolean process(Set<? extends TypeElement> annotations, RoundEnvironment roundEnv)
	{
		annotations.stream().forEach(annotationElement -> {
			roundEnv.getElementsAnnotatedWith(annotationElement).stream()
					.filter(element -> element.getKind().equals(ElementKind.CLASS))
					.forEach(element -> {
						ClassDescriptor descriptor = new ClassDescriptor();

						//getting entity name
						TypeElement classElement = (TypeElement) element;
						String className = classElement.getSimpleName().toString();
						descriptor.setClassName(className);
						descriptor.setFullClassName(classElement.getQualifiedName().toString());

						//getting package where entity is
						PackageElement packageElement = (PackageElement) element.getEnclosingElement();
						String packageName = packageElement.getQualifiedName().toString();
						descriptor.setPackageName(packageName);

						//getting absent annotation field necessity strategy from annotation
						AbsentAnnotationFieldNecessity absentAnnotationFieldNecessity;
						Annotation annotation = null;
						try
						{
							Class annotationClass = Class.forName(annotationElement.getQualifiedName().toString());
							annotation = element.getAnnotation(annotationClass);
							absentAnnotationFieldNecessity = (AbsentAnnotationFieldNecessity) annotationClass
									.getMethod("absentAnnotationFieldNecessity")
									.invoke(annotation);
						}
						catch (Exception e)
						{
							logger.error(e.getMessage());
							absentAnnotationFieldNecessity = AbsentAnnotationFieldNecessity.MANDATORY;
						}

						//getting list of mandatory and optional fields for further processing and building the descriptor
						List<VariableElement> mandatoryFields = new ArrayList<>();
						List<VariableElement> optionalFields = new ArrayList<>();
						processFields(element, absentAnnotationFieldNecessity, mandatoryFields, optionalFields);

						//element field collection is always randomly ordered, that's why it should be manually sorted to
						// get constant ordering
						Collections.sort(mandatoryFields,
								(f1, f2) -> {
									int f1OrderNumber = 0;
									int f2OrderNumber = 0;

									Mandatory f1Mandatory = f1.getAnnotation(Mandatory.class);
									if (f1Mandatory != null)
									{
										f1OrderNumber = f1Mandatory.orderNumber();
									}

									Mandatory f2Mandatory = f2.getAnnotation(Mandatory.class);
									if (f2Mandatory != null)
									{
										f2OrderNumber = f2Mandatory.orderNumber();
									}

									String f1Name = f1.getSimpleName().toString();
									String f2Name = f2.getSimpleName().toString();

									if (f1OrderNumber == 0 && f2OrderNumber == 0)
									{
										return f1Name.compareTo(f2Name);
									}

									if (f1OrderNumber == f2OrderNumber)
									{
										logger.warn("[" + className + "] " + "Fields " + f1Name + " and " +
												f2Name + " got equal order numbers. They are sorted alphabetically");
										return f1Name.compareTo(f2Name);
									}

									return f1OrderNumber > f2OrderNumber ? 1 : -1;
								});
						Collections.sort(optionalFields,
								(f1, f2) -> f1.getSimpleName().toString().compareTo(f2.getSimpleName().toString()));

						//building descriptor
						processMandatoryFields(mandatoryFields, descriptor);
						processOptionalFields(optionalFields, descriptor);

						//if object is annotated with SpringBuildObject annotation, then generated Builder class is
						// going to be a Spring Component
						if (annotation != null && SpringBuildObject.class.equals(annotation.annotationType()))
						{
							descriptor.setSpringComponent(true);
							descriptor.setSpringComponentName(((SpringBuildObject) annotation).value());
						}

						//creating a source file for generated Builder class and writing the content of a template with
						// descriptor values
						try
						{
							JavaFileObject fileObject = processingEnv.getFiler()
									.createSourceFile(packageName + ".builder." + className + "Builder");
							Writer writer = fileObject.openWriter();
							TemplateWriter.getInstance().setLogger(logger).write(writer, descriptor);
							writer.close();
						}
						catch (IOException e)
						{
							logger.error("IOException occurred: " + e.getMessage(), element);
						}
					});
		});
		return false;
	}

	/**
	 * Processes fields of a given class and divides them into mandatory and optional
	 *
	 * @param classElement
	 * 		class to process fields
	 * @param mandatoryFields
	 * 		list to store mandatory fields
	 * @param optionalFields
	 * 		list to store optional fields
	 */
	private void processFields(Element classElement, AbsentAnnotationFieldNecessity absentAnnotationFieldNecessity,
							   List<VariableElement> mandatoryFields, List<VariableElement> optionalFields)
	{
		classElement.getEnclosedElements().stream()
				.filter(innerElement -> innerElement.getKind().equals(ElementKind.FIELD)) //process only fields
				.filter(innerElement -> innerElement.getAnnotation(Ignore.class) ==
						null) //process fields that are not marked as ignored
				.forEach(innerElement -> {
					VariableElement fieldElement = (VariableElement) innerElement;

					//if BuildObject#absentAnnotationFieldNecessity was set as Mandatory than fields without Optional
					// annotation and are not explicitly annotated with Mandatory annotation are treated as they were
					// annotated with Mandatory annotation
					if (absentAnnotationFieldNecessity.equals(AbsentAnnotationFieldNecessity.MANDATORY))
					{
						if (fieldElement.getAnnotation(Mandatory.class) != null ||
								fieldElement.getAnnotation(Optional.class) == null)
						{
							mandatoryFields.add(fieldElement);
						}
						else
						{
							optionalFields.add(fieldElement);
						}
					}
					//and if it was set as Optional than fields without Mandatory annotation and are not explicitly
					// annotated with Optional annotation are treated as they were annotated with Optional annotation
					else if (absentAnnotationFieldNecessity.equals(AbsentAnnotationFieldNecessity.OPTIONAL))
					{
						if (fieldElement.getAnnotation(Optional.class) != null ||
								fieldElement.getAnnotation(Mandatory.class) == null)
						{
							optionalFields.add(fieldElement);
						}
						else
						{
							mandatoryFields.add(fieldElement);
						}
					}
				});
	}

	/**
	 * Processes mandatory fields and builds descriptor
	 *
	 * @param mandatoryFields
	 * 		list of mandatory fields
	 * @param descriptor
	 * 		builder descriptor
	 */
	private void processMandatoryFields(List<VariableElement> mandatoryFields, ClassDescriptor descriptor)
	{
		//processing begins from the last mandatory field. every setter returns current instance of builder, but
		// return type is defined as one of nested interfaces that define one setter for mandatory field and several
		// for optional
		String returnType = NestedInterfaceDescriptor.OPTIONAL_FIELD_INTERFACE_NAME;
		for (int i = mandatoryFields.size() - 1; i >= 0; i--)
		{
			VariableElement field = mandatoryFields.get(i);
			String fieldName = field.getSimpleName().toString();

			FieldDescriptor fieldDescriptor = new FieldDescriptor();
			fieldDescriptor.setName(fieldName);
			fieldDescriptor
					.setType(getTypeSimpleNameAndImportPackages(field.asType().toString(), descriptor.getPackages()));
			descriptor.getFields().add(fieldDescriptor);

			SetterDescriptor setterDescriptor = new SetterDescriptor();
			setterDescriptor.setReturnType(returnType);
			setterDescriptor.setField(fieldDescriptor);

			descriptor.getSetters().add(setterDescriptor);

			NestedInterfaceDescriptor interfaceDescriptor = new NestedInterfaceDescriptor();
			interfaceDescriptor.setFieldName(fieldName);
			interfaceDescriptor.getSetters().add(setterDescriptor);
			descriptor.getInterfaces().add(interfaceDescriptor);

			returnType = interfaceDescriptor.getInterfaceName();
		}
		//reverse the class list, because fields were processed form the last one
		Collections.reverse(descriptor.getInterfaces());
	}

	/**
	 * Process optional fields and build descriptor
	 *
	 * @param optionalFields
	 * 		list of optional fields
	 * @param descriptor
	 * 		builder descriptor
	 */
	private void processOptionalFields(List<VariableElement> optionalFields, ClassDescriptor descriptor)
	{
		//optional field setters are placed in one class that implements Builder interface so it has the build()
		// method that will return an instance of an entity
		NestedInterfaceDescriptor interfaceDescriptor = new NestedInterfaceDescriptor();
		interfaceDescriptor.setOptionalFieldInterface(true);

		for (VariableElement optionalField : optionalFields)
		{
			String fieldName = optionalField.getSimpleName().toString();

			FieldDescriptor fieldDescriptor = new FieldDescriptor();
			fieldDescriptor.setName(fieldName);
			fieldDescriptor.setType(
					getTypeSimpleNameAndImportPackages(optionalField.asType().toString(), descriptor.getPackages()));
			descriptor.getFields().add(fieldDescriptor);

			SetterDescriptor setterDescriptor = new SetterDescriptor();
			setterDescriptor.setField(fieldDescriptor);
			setterDescriptor.setReturnType(NestedInterfaceDescriptor.OPTIONAL_FIELD_INTERFACE_NAME);
			interfaceDescriptor.getSetters().add(setterDescriptor);
			descriptor.getSetters().add(setterDescriptor);
		}

		descriptor.getInterfaces().add(interfaceDescriptor);
	}

	private String getTypeSimpleNameAndImportPackages(String type, Collection<String> importedPackages)
	{
		int simpleNameStartIndex = type.lastIndexOf('.');

		if (simpleNameStartIndex == -1)
		{
			return type;
		}

		String packageName = type.substring(0, simpleNameStartIndex);
		if (!importedPackages.contains(packageName) && !packageName.equals("java.lang"))
		{
			importedPackages.add(packageName);
		}
		return type.substring(simpleNameStartIndex + 1);
	}
}
