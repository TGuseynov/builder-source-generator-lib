/*
 * Copyright (c) Watcher, 2015.
 */

package com.gitlab.tguseynov.sourcegeneratorlib.processor;

import com.gitlab.tguseynov.sourcegeneratorlib.processor.velocity.VelocityFacade;
import com.gitlab.tguseynov.sourcegeneratorlib.processor.descriptor.ClassDescriptor;
import com.gitlab.tguseynov.sourcegeneratorlib.processor.logger.AnnotationProcessorLogger;

import java.io.IOException;
import java.io.Writer;
import java.net.URL;
import java.util.Properties;

public class TemplateWriter
{
	private static TemplateWriter instance = null;

	private Properties properties;
	private AnnotationProcessorLogger logger;

	private TemplateWriter()
	{

	}

	/**
	 * Generates and writes a source file
	 *
	 * @param writer
	 * 		instance of a writer
	 * @param descriptor
	 * 		builder class descriptor
	 *
	 * @return <b>TRUE</b> if source was successfully written
	 *
	 * @see ClassDescriptor
	 */
	public boolean write(Writer writer, ClassDescriptor descriptor)
	{
		return VelocityFacade.getInstance().mergeTemplate(writer, descriptor, getProperties());
	}

	/**
	 * @param logger
	 * 		{@link AnnotationProcessorLogger}
	 *
	 * @return instance for chain call
	 */
	public TemplateWriter setLogger(AnnotationProcessorLogger logger)
	{
		this.logger = logger;
		return this;
	}

	/**
	 * Parses Velocity properties file
	 *
	 * @return parsed Velocity properties
	 */
	private Properties getProperties()
	{
		if (properties == null)
		{
			properties = new Properties();
			URL propertiesUrl = getClass().getClassLoader().getResource(VelocityFacade.VELOCITY_PROPERTIES_FILE_NAME);
			if (propertiesUrl != null)
			{
				try
				{
					properties.load(propertiesUrl.openStream());
				}
				catch (IOException e)
				{
					if (logger != null)
					{
						logger.warn("IOException occurred on reading velocity properties file " +
								VelocityFacade.VELOCITY_PROPERTIES_FILE_NAME + ": " + e.getMessage());
					}
				}
			}
		}
		return properties;
	}

	/**
	 * @return {@link TemplateWriter} instance
	 */
	public static TemplateWriter getInstance()
	{
		if (instance == null)
		{
			instance = new TemplateWriter();
		}
		return instance;
	}
}
