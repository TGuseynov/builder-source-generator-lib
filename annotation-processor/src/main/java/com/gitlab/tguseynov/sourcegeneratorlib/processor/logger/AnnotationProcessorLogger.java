package com.gitlab.tguseynov.sourcegeneratorlib.processor.logger;

import javax.annotation.processing.Messager;
import javax.lang.model.element.Element;
import javax.tools.Diagnostic;

/**
 * Logger facade for annotation processor. An instance of {@link Messager} should be set to get logger working
 *
 * @see javax.annotation.processing.ProcessingEnvironment#getMessager() ProcessingEnvironment#getMessager()
 * @see #setMessager(Messager)
 */
public class AnnotationProcessorLogger
{
	private static AnnotationProcessorLogger instance = null;

	private Messager messager;

	private AnnotationProcessorLogger()
	{

	}

	/**
	 * @param messager
	 * 		instance of {@link Messager} to log messages
	 */
	public void setMessager(Messager messager)
	{
		this.messager = messager;
	}

	/**
	 * Prints warning message
	 *
	 * @param message
	 * 		message to log
	 *
	 * @see javax.tools.Diagnostic.Kind#WARNING
	 */
	public void warn(String message)
	{
		log(Diagnostic.Kind.WARNING, message);
	}

	/**
	 * Prints warning message with hint to element's position
	 *
	 * @param message
	 * 		message to log
	 * @param element
	 * 		element that caused warning
	 *
	 * @see javax.tools.Diagnostic.Kind#WARNING
	 */
	public void warn(String message, Element element)
	{
		log(Diagnostic.Kind.WARNING, message, element);
	}

	/**
	 * Prints error message
	 *
	 * @param message
	 * 		message to log
	 *
	 * @see javax.tools.Diagnostic.Kind#ERROR
	 */
	public void error(String message)
	{
		log(Diagnostic.Kind.ERROR, message);
	}

	/**
	 * Prints error message with hint to element's position
	 *
	 * @param message
	 * 		message to log
	 * @param element
	 * 		element that caused error
	 *
	 * @see javax.tools.Diagnostic.Kind#ERROR
	 */
	public void error(String message, Element element)
	{
		log(Diagnostic.Kind.ERROR, message, element);
	}

	/**
	 * Prints message with given level
	 *
	 * @param level
	 * 		{@link javax.tools.Diagnostic.Kind Logger level}
	 * @param message
	 * 		message to log
	 */
	public void log(Diagnostic.Kind level, String message)
	{
		messager.printMessage(level, message);
	}

	/**
	 * Prints message with given level with hint to element's position
	 *
	 * @param level
	 * 		{@link javax.tools.Diagnostic.Kind Logger level}
	 * @param message
	 * 		message to log
	 * @param element
	 * 		element that caused logging
	 */
	public void log(Diagnostic.Kind level, String message, Element element)
	{
		messager.printMessage(level, message, element);
	}

	/**
	 * @return {@link AnnotationProcessorLogger} instance
	 */
	public static AnnotationProcessorLogger getInstance()
	{
		if (instance == null)
		{
			instance = new AnnotationProcessorLogger();
		}
		return instance;
	}
}
