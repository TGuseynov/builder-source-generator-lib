package com.gitlab.tguseynov.sourcegeneratorlib.processor.descriptor;

import java.util.ArrayList;
import java.util.List;

public class NestedInterfaceDescriptor
{
	public static final String INTERFACE_PREFIX = "I";
	public static final String OPTIONAL_FIELD_INTERFACE_NAME = INTERFACE_PREFIX + "Optional";

	private String fieldName;
	private List<SetterDescriptor> setters;
	private boolean optionalFieldInterface;

	public NestedInterfaceDescriptor()
	{
		setters = new ArrayList<>();
	}

	public String getFieldName()
	{
		return fieldName;
	}

	public void setFieldName(String fieldName)
	{
		this.fieldName = fieldName;
	}

	public String getInterfaceName()
	{
		return fieldName == null ? OPTIONAL_FIELD_INTERFACE_NAME :
				INTERFACE_PREFIX + fieldName.substring(0, 1).toUpperCase() + fieldName.substring(1);
	}

	public List<SetterDescriptor> getSetters()
	{
		return setters;
	}

	public void setSetters(
			List<SetterDescriptor> setters)
	{
		this.setters = setters;
	}

	public boolean getOptionalFieldInterface()
	{
		return optionalFieldInterface;
	}

	public void setOptionalFieldInterface(boolean optionalFieldInterface)
	{
		this.optionalFieldInterface = optionalFieldInterface;
	}
}
