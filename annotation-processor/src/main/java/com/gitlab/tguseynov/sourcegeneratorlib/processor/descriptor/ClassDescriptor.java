package com.gitlab.tguseynov.sourcegeneratorlib.processor.descriptor;

import com.gitlab.tguseynov.sourcegeneratorlib.processor.SourceGeneratorAnnotationProcessor;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;

/**
 * Descriptor for {@link SourceGeneratorAnnotationProcessor SourceGeneratorAnnotationProcessor} that contains all the info about
 * generated builder class
 */
public class ClassDescriptor
{
	private String packageName;
	private String className;
	private String fullClassName;

	private Set<String> packages;
	
	private boolean springComponent;
	private String springComponentName;

	private List<FieldDescriptor> fields;
	private List<SetterDescriptor> setters;
	private List<NestedInterfaceDescriptor> interfaces;

	public ClassDescriptor()
	{
		packages = new TreeSet<>();
		fields = new ArrayList<>();
		setters = new ArrayList<>();
		interfaces = new ArrayList<>();

		springComponent = false;
	}

	/**
	 * @return package of generated builder class
	 */
	public String getPackageName()
	{
		return packageName;
	}

	/**
	 * @param packageName
	 * 		package of generated builder class
	 */
	public void setPackageName(String packageName)
	{
		this.packageName = packageName;
	}

	/**
	 * @return class name of entity builder class is generated for
	 */
	public String getClassName()
	{
		return className;
	}

	/**
	 * @param className
	 * 		class name of entity builder class is generated for
	 */
	public void setClassName(String className)
	{
		this.className = className;
	}

	/**
	 * @return fully qualified class name of entity builder class is generated for
	 */
	public String getFullClassName()
	{
		return fullClassName;
	}

	/**
	 * @param fullClassName
	 * 		fully qualified class name of entity builder class is generated for
	 */
	public void setFullClassName(String fullClassName)
	{
		this.fullClassName = fullClassName;
	}

	/**
	 * @return list of imported packages
	 */
	public Set<String> getPackages()
	{
		return packages;
	}

	/**
	 * @param packages
	 * 		list of imported packages
	 */
	public void setPackages(Set<String> packages)
	{
		this.packages = packages;
	}

	/**
	 * @return determines whether described builder instance is a Spring Component
	 */
	public boolean getSpringComponent()
	{
		return springComponent;
	}

	/**
	 * @param springComponent
	 * 		determines whether described builder instance is a Spring Component
	 */
	public void setSpringComponent(boolean springComponent)
	{
		this.springComponent = springComponent;
	}

	/**
	 * @return suggested name for a Spring Component
	 */
	public String getSpringComponentName()
	{
		return springComponentName;
	}

	/**
	 * @param springComponentName
	 * 		suggested name for a Spring Component
	 */
	public void setSpringComponentName(String springComponentName)
	{
		this.springComponentName = springComponentName;
	}

	/**
	 * @return list of setters
	 *
	 * @see SetterDescriptor
	 */
	public List<SetterDescriptor> getSetters()
	{
		return setters;
	}

	/**
	 * @param setters
	 * 		list of setters
	 *
	 * @see SetterDescriptor
	 */
	public void setSetters(List<SetterDescriptor> setters)
	{
		this.setters = setters;
	}

	/**
	 * @return list of entity fields
	 *
	 * @see FieldDescriptor
	 */
	public List<FieldDescriptor> getFields()
	{
		return fields;
	}

	/**
	 * @param fields
	 * 		list of entity fields
	 *
	 * @see FieldDescriptor
	 */
	public void setFields(List<FieldDescriptor> fields)
	{
		this.fields = fields;
	}

	/**
	 * @return list of inner builder interfaces
	 *
	 * @see NestedInterfaceDescriptor
	 */
	public List<NestedInterfaceDescriptor> getInterfaces()
	{
		return interfaces;
	}

	/**
	 * @param interfaces
	 * 		list of inner builder interfaces
	 *
	 * @see NestedInterfaceDescriptor
	 */
	public void setInterfaces(List<NestedInterfaceDescriptor> interfaces)
	{
		this.interfaces = interfaces;
	}
}
