1. Clone this repository

    `git clone git@gitlab.com:TGuseynov/source-generator-lib.git`
2. Create new branch

    `git checkout -b {type}/{branch_name}`,

     where `{type}` can be `feature` or `bugfix`
4. Make changes and commit them
    `git commit -m "commit_message"`
4. Push your branch to repository
    `git push origin {type}/{branch_name}`
5. Submit a merge request