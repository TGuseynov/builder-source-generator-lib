- **v. 1.1**
    - Fixed NullPointerException randomly thrown during class generation
    - Fixed order number not taken into account
    - Add spring support
    - Generated classes are more readable
    - Performance improvement

- **v. 1.0.1**
    - Fixed critical bug whe Builder class has no finish() method and instantiated object cannot be retrieved

- **v. 1.0**
    - First release