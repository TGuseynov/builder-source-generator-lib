/*
 * Copyright (c) Watcher, 2015.
 */

package com.gitlab.tguseynov.sourcegeneratorlib.builder.annotation;

/**
 * Enumeration describes how fields without builder annotations ({@link Mandatory}, {@link Optional}, {@link Ignore})
 * should be treated in a class marked with {@link BuildObject} annotation
 */
public enum AbsentAnnotationFieldNecessity
{
	/**
	 * Indicates that fields without annotation are mandatory
	 */
	MANDATORY,
	/**
	 * Indicates that fields without annotation are optional
	 */
	OPTIONAL
}
