/*
 * Copyright (c) Watcher, 2015.
 */

package com.gitlab.tguseynov.sourcegeneratorlib.builder;

import com.gitlab.tguseynov.sourcegeneratorlib.builder.annotation.BuildObject;

/**
 * Interface for generated builder class
 *
 * @param <T>
 * 		class, which instance would be built
 *
 * @see BuildObject BuildObject
 */
public interface Builder<T>
{
	/**
	 * Build an object
	 *
	 * @return built object
	 */
	T build();
}
