package com.gitlab.tguseynov.sourcegeneratorlib.builder.annotation;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * Annotation used to indicate that a builder class should be generated for annotated class.
 * <p>
 * Builder class would be marked as Spring Component and would be available as bean. You can suggest the name of the
 * bean by setting {@link #value() annotation value}
 *
 * @see <a href="http://docs.spring.io/autorepo/docs/spring-framework/3.0.x/javadoc-api/org/springframework/stereotype/Component.html">Spring
 * Component annotation documentation</a>
 */
@Retention(RetentionPolicy.SOURCE)
@Target({ElementType.TYPE})
public @interface SpringBuildObject
{
	/**
	 * @return fields without builder annotation treating strategy. Such fields are treated as mandatory by default
	 *
	 * @see AbsentAnnotationFieldNecessity
	 */
	AbsentAnnotationFieldNecessity absentAnnotationFieldNecessity() default AbsentAnnotationFieldNecessity.MANDATORY;

	/**
	 * @return a suggestion for a Spring bean name
	 */
	String value() default "";
}
