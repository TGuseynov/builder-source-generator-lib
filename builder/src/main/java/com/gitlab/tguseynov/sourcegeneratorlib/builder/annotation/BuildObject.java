/*
 * Copyright (c) Watcher, 2015.
 */

package com.gitlab.tguseynov.sourcegeneratorlib.builder.annotation;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * Annotation used to indicate that a builder class should be generated for annotated class.
 *
 * BuilderAnnotationProcessor#process
 */
@Retention(RetentionPolicy.SOURCE)
@Target({ElementType.TYPE})
public @interface BuildObject
{
	/**
	 * @return fields without builder annotation treating strategy. Such fields are treated as mandatory by default
	 *
	 * @see AbsentAnnotationFieldNecessity
	 */
	AbsentAnnotationFieldNecessity absentAnnotationFieldNecessity() default AbsentAnnotationFieldNecessity.MANDATORY;
}
