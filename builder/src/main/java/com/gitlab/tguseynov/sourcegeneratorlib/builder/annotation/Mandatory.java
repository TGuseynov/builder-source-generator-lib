/*
 * Copyright (c) Watcher, 2015.
 */

package com.gitlab.tguseynov.sourcegeneratorlib.builder.annotation;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * Annotation indicates mandatory fields
 */
@Retention(RetentionPolicy.SOURCE)
@Target({ElementType.FIELD})
public @interface Mandatory
{
	/**
	 * @return Number of a position field should be placed in a generated class
	 */
	int orderNumber() default 0;
}
