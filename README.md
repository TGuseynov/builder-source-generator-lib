# Source Generator Library

A library that generates Builder classes for annotated POJOs. Annotations are processed and Builder class is generated
while compiling by annotation processor.

Project consists of 2 modules:
* builder
* annotation-processor

Builder module contains annotations for a POJO class and a generic Builder interface that every generated class
implements.
Annotation-processor module contains the annotation processor itself.
Gradle-plugin module is a gradle plugin that places generated sources into src/main/generated folder and marks it as a
source root.

# Installation
* **Gradle**

    ```
    repositories {
        mavenCentral()
    }

    dependencies {
        compile group: 'com.gitlab.tguseynov.buildersourcegeneratorlib', name: 'builder', version: '1.0.1'
        compile group: 'com.gitlab.tguseynov.buildersourcegeneratorlib', name: 'annotation-processor', version: '1.0.1'
    }
    ```

# Usage

A desired class should be annotated with `@BuildObject` in order to be processed by `BuilderAnnotationProcessor`, that
is generating appropriate Builder class. You can mark field either mandatory (`@Mandatory`) or optional (`@Optional`).
You can also mark field as ignored with `@Ignore` annotation. `@BuildObject` annotation has property
`absentAnnotationFieldNecessity`, how to implicitly treat fields without annotations, by default they are implicitly
mandatory.

Generated Builder class creates some kind of DSL, where you are forced to set mandatory values to get to the `build()`
method to get instantiated object.

# Example

POJO:
```
@BuildObject
public class Example
{
    private String mandatoryField;
    private String anotherMandatoryField;
    @Optional
    private String optionalField;
    @Optional
    private String anotherOptionalField;

    public Example()
    {
    }

    public String getMandatoryField()
    {
        return mandatoryField;
    }

    public void setMandatoryField(String mandatoryField)
    {
        this.mandatoryField = mandatoryField;
    }
    ...
}
```

A Builder class is generated and is compiled then. Usage of Builder class:
```
Example example = ExampleBuilder.start()
                                .setMandatoryField("string")
                                .setAnotherMandatoryField("another string")
                                .setOptionalField("optional")
                                .finish()
                                .build();
```

In order to get instance of a class you should call the `build()` methods that is only available after you call
`finish()` method, that becomes available when all mandatory fields are set.